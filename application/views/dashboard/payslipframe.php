<!DOCTYPE html>
<html>
<head>
  <title>Payslip</title>
  <link rel="stylesheet" href="../assets/css/vendor.css">
  <link rel="stylesheet" href="../assets/css/chl.css">
  <link id="theme-list" rel="stylesheet" href="../assets/css/theme-peter-river.css">
</head>
<style type="text/css">
  table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }
  td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
  }
</style>
<body>
<div class="logo">
              <!--begin logo-->
              <div class="logo">
                  <img src="../assets/img/logo_gapura.png">
              </div>
              <!--end logo-->
            </div>
</body>
</html>
<?php
// require_once('libs\saprfc.php');
require_once('..\assets\libs\sap.php');
$sap = new SAPConnection();
$sap->Connect("..\assets\libs\logon_data.conf");

if($sap->getStatus() == SAPRFC_OK) $sap->open();
if ($sap->GetStatus() != SAPRFC_OK ) {
       #$sap->PrintStatus();
       exit;
    }

// Inisialisasi variabel
$noPeg;
$nowDate = (string)date("d.m.Y");
$beginDate = "01.01.2015";
$endDate = "31.12.9999";
$seqNumber = array();
$payslipVar = "GAPURA";

// data sementaranya
$noPeg = "2000000";
// var_dump($beginDate);


// cek dia masih pegawai Aktif atau tidak
$fce = $sap->NewFunction ("ZHR_GET_EMPLOYEE_DATA");
    if ($fce == false ) {
       // $sap->PrintStatus();
       exit;
    }

$fce->PERSON_ID=$noPeg;
$fce->SELECTION_BEGIN=$nowDate;
$fce->SELECTION_END=$nowDate;


$fce->Call();
if($fce->GetStatus() == SAPRFC_OK){
  if($fce->PERSONAL_DATA["STATUS"]!="3"){
    echo "Anda bukan pegawai aktif";
    exit;
  }
}
else{
  $fce->PrintStatus();
  echo "gajalan";
}


// //ambil sequence number
$fce = $sap->NewFunction ("BAPI_GET_PAYROLL_RESULT_LIST");
    if ($fce == false ) {
       // $sap->PrintStatus();
       exit;
    }

$fce->EMPLOYEENUMBER=$noPeg;
$fce->FROMDATE=$beginDate;
$fce->TODATE=$endDate;

$fce->Call();
if(isset($fce->RESULTS)){
	$fce->RESULTS->Reset();
	 while ($fce->RESULTS->next()) {
 		// echo $fce->RESULTS->row["SEQUENCENUMBER"]."<br>";
 		array_push($seqNumber, $fce->RESULTS->row["SEQUENCENUMBER"]);
 	}
}
else{
	$fce->PrintStatus();	
}
// echo $seqNumber;

// Tampilkan payslip
$fce = $sap->NewFunction ("BAPI_GET_PAYSLIP_HTML");
    if ($fce == false ) {
       // $sap->PrintStatus();
       exit;
    }

$fce->EMPLOYEENUMBER=$noPeg;
$fce->SEQUENCENUMBER=$seqNumber[1];
$fce->PAYSLIPVARIANT=$payslipVar;

$fce->Call();
 if($fce->GetStatus()== SAPRFC_OK){
 	$fce->PAYSLIP_HTML->Reset();
 	// var_dump($fce->PAYSLIP_HTML);
 	while ($fce->PAYSLIP_HTML->next()) {
 		echo $fce->PAYSLIP_HTML->row["LINE"];
 	}
 }
 else{
 	$fce->PrintStatus();
 }
 ?>