<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home extends CI_Controller {
	
	public function profile(){
		$this->load->model('User');
		$nopeg = $this->session->userdata('uName');
            if($nopeg == false){
                redirect(base_url());
            }
        //check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($nopeg);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
		if($nopeg == 'adminsm'||$nopeg == 'adminhc'){
			$photo = base_url().'assets/img/admin.png';
			$data['foto'] = $photo;
		}
		else{
			$photo = $this->User->getPhoto($this->session->userdata('uName'));
			if($photo == 1){
				$photo = base_url().'assets/img/admin.png';
			}
			$data['foto'] = $photo;
		}
		$otorisasi = $this->User->setOtorisasi($this->session->userdata('uName'));
		// Ambil data keluarga
		$family = $this->User->getFamily($this->session->userdata('uName'));
		if($family == 0){
			$data['family'] = "<div class='alert alert-warning'><b>You have no dependants</b></div>";
			// $this->session->set_flashdata('responseFamily',"<div class='alert alert-warning'><b>You have no dependants</b></div>");
		}
		else{
			$data['family'] = $this->User->getFamily($this->session->userdata('uName'));	
		}
		$data['otorisasi'] = $otorisasi;
		$this->load->view('dashboard/profile',$data);	
	}

	public function dashboard(){
		redirect('/dashboard/');
	}

	public function changePassword(){
		$session = $this->session->userdata('uName');
            if($session == false){
                redirect(base_url());
            }
        $this->load->model('User');
        //check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}    
		if(!isset($_POST['submit'])){
			$passwordLama=$_POST['passwordLama'];
			$passwordBaru=$_POST['passwordBaru'];
			$passwordBaruConfirm=$_POST['passwordBaruConfirm'];
			if($passwordBaru!=$passwordBaruConfirm){
				//$data['errorMsg'] = "Password baru dan konfirmasi password baru tidak sama";
				$this->profile();
				$this->session->set_flashdata('response',"<div class='alert alert-warning'><b class='text-center'>Password baru dan konfirmasi password baru tidak sama</b></div>");
				redirect('/home/profile');
				//echo '<div id="form-submit-alert">Password baru dan konfirmasi password baru tidak sama</div>';
			}
			else{
				$return = $this->User->gantiPassword($this->session->userdata['uName'], $passwordLama, $passwordBaru);
				if($return != 1){
					$this->profile();
					$this->session->set_flashdata('response',"<div class='alert alert-warning'><b class='text-center'>Anda salah memasukan password lama</b></div>");
					redirect('/home/profile');

					//$data['errorMsg'] = "Anda salah memasukkan password lama";
					//$this->load->view('error',$data);
					//echo "<script>setTimeout(\"location.href = '".base_url()."/index.php/home/profile';\",1500);</script>";
				}
				else{
					$this->profile();	
					$this->session->set_flashdata('response',"<div class='alert alert-success fade in'><b>Ganti password sukses</b></div>");
					redirect('/home/profile');
				}
			}
		}
	}

	public function news(){
		$this->load->model('User');
		$session =$this->session->userdata('uName'); 
            // var_dump($this->session->userdata);
        if($session == false){
            redirect(base_url());
        }
        //check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		} 
		$otorisasi = $this->User->setOtorisasi($this->session->userdata('uName'));
		$data['otorisasi'] = $otorisasi;
        $photo = $this->User->getPhoto($session);
		$data['foto'] = $photo;

		$this->load->view('dashboard/news',$data);
	}
}
?>