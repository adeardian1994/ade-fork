<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class administrative extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->library('excel');//load PHPExcel Library
		//$this->load->model('upload_model');//To Upload file in directory
		$this->load->model('Administrative_Model');
		
	}

	public function ambilOtorisasi(){
		$this->load->model('User');
		$nopeg = $_POST['nopeg'];
		$otorisasi = $this->User->ambilOtorisasi($nopeg);
		echo json_encode($otorisasi);
		// echo "<script>console.log('masuk dari php')</script>";
		// return $otorisasi;
	}

	public function index(){
		$session =$this->session->userdata('uName'); 
            // var_dump($this->session->userdata);
        if($session == false){
            redirect(base_url());
        }
        $this->load->model('User');
        $otorisasi = $this->User->setOtorisasi($this->session->userdata('uName'));
        if($otorisasi['ADM'] !=1){
        	redirect(base_url());
        }
        //check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
        $photo = $this->User->getPhoto($session);
		$data['otorisasi'] = $otorisasi;
		$data['foto'] = $photo;
        $data['nopeg'] = $this->session->userdata('uName');
        $this->load->model("Administrative_Model");
        $data['users'] = $this->Administrative_Model->getAllUser();
		$this->load->view('dashboard/administrative',$data);
	}
	
	public function deleteUser(){
		$this->load->model('User');
		$this->load->model('Administrative_Model');
		$session =$this->session->userdata('uName');
            if($session == false){
                redirect(base_url());
            }
        //check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		} 
		$username = $_POST['username'];
		$this->load->model('User');
		$this->load->model('Administrative_Model');
		$useravailability = $this->User->cekUser($username);
		// kondisi field username yang akan dihapus kosong
		if ($username == NULL) {
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Please input username first!</b></div>");
			redirect('/administrative/');
		}
		// kondisi field username adalah username user ang sedang aktif(hapus akun sendiri)
		if ($session == $username) {
			$this->session->set_flashdata('response',"<div class='alert alert-danger'><b>You can't delete yourself!</b></div>");
			redirect('/administrative/');
		}
		// kondisi penghapusan yang valid, akun ini menghapus akun lain
		if ($useravailability == $username) {
			$this->Administrative_Model->deleteUser($username);
			$this->session->set_flashdata('response',"<div class='alert alert-danger'><b>User $username deleted!</b></div>");
			redirect('/administrative/');
		}
		// Kondisi field username berisi username yang tidak ada di database
		elseif ($useravailability != $username) {
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>There is no such username as '".$username."' !</b></div>");
			redirect('/administrative/');
		}
	}

	public function deleteMpyUser(){
		$session =$this->session->userdata('uName');
            if($session == false){
                redirect(base_url());
            }

        $this->load->model('User');
		$this->load->model('Administrative_Model');
		//check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
		//path of filesnya
		$config = array(
			'upload_path' => "./upload/excel",
			'allowed_types' => "xlsx",
			'overwrite' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)

			);
			$this->load->library('upload', $config);
			if($this->upload->do_upload())
			{
			$data = array('upload_data' => $this->upload->data());
			$file_name = $data['upload_data']['file_name']; //nama filenya
			}
			else
			{
				$this->session->set_flashdata('response',"<div class='alert alert-danger'><b>Please use excel files only!</b></div>");
				redirect('/administrative/');
				$error = array('error' => $this->upload->display_errors());
			} //returns array yg menyimpan seluruh data dari file
		
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		//read only
		$objReader->setReadDataOnly(true);
		//load excel file
		$objPHPExcel = $objReader->load('upload/excel/'.$file_name);
		//jadi kalo gini bisa tapi cuma 1 data aja yang ke input ke dalem SQLnya
		$totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();//hitung banyak row
		$alertrows = $totalrows - 1;
		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		$ctr=0;
		//loop data dalem excel
		for ($i=2; $i<=$totalrows; $i++) { 
			$username = strip_tags($objWorksheet->getCellByColumnAndRow(0,$i)->getValue());
			$useravailability = $this->User->cekUser($username);
			// kondisi field username yang akan dihapus kosong
			if ($username == NULL) {
				$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>$ctr users deleted! But error occured in the following column. Please fill every column in your excel files correctly!</b></div>");
				redirect('/administrative/');
			}
			// kondisi field username adalah username user ang sedang aktif(hapus akun sendiri)
			if ($session == $username) {
				$this->session->set_flashdata('response',"<div class='alert alert-danger'><b>$ctr users deleted! But error occured in the following column. Because you can't delete yourself! and We also stop the deletion process from username $username! please update your excel files!</b></div>");
			redirect('/administrative/');
			}
			// Kondisi field username berisi username yang tidak ada di database
			if ($useravailability != $username) {
				$this->session->set_flashdata('response',"<div class='alert alert-danger'><b>$ctr users deleted! But error occured in the following column. Because there is no such username as '".$username."' ! and We also stop the deletion process from username $username! please update your excel files!</b></div>");
				redirect('/administrative/');
			}
			if ($useravailability == $username) {
				$this->Administrative_Model->deleteUser($username);
			}
				$ctr++; 
		}
		unlink('upload/excel/'.$file_name); //delete file setelah upload ke db
		$this->session->set_flashdata('response',"<div class='alert alert-success'><b>Succesfully Deleted $alertrows Users!</b></div>");
		redirect('/administrative/');
	}

	public function lockUser(){
		$session =$this->session->userdata('uName');
            if($session == false){
                redirect(base_url());
            } 
		$username = $_POST['lock'];
		$this->load->model('Administrative_Model');
		$this->load->model('User');
		//check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
		$status = $this->User->cekStatus($username);
		if ($status == 'AKTIF') {
			$this->Administrative_Model->lock($username);
		} elseif($status == 'NON AKTIF'){
			$this->Administrative_Model->unLock($username);
		}	
		$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>You've Change User '".$username."' Status</b></div>");
		redirect('/administrative/');
	}

	public function promoteUser(){
		$session =$this->session->userdata('uName');
            if($session == false){
                redirect(base_url());
            }
		$this->load->model('Administrative_Model');
		$this->load->model('User');
		//check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
		// ambil data lewat post
		$userid = $_POST['userid'];
		$yesADM = $_POST['ADM'];
		$yesHCM = $_POST['HCM'];
		if($yesADM == 1 && $yesHCM == 1){
			$authorization = "ADM-HCM";
		}
		else if ($yesADM == 1) {
			$authorization = "ADM";
		}
		else if ($yesHCM == 1) {
			$authorization = "HCM";
		}
		else{
			$authorization = "PEG";
		}

		//check authorization dan 
		$otorisasi = $this->User->setOtorisasi($userid);
		if ($otorisasi == $authorization) {
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>The user $userid Authorization still the same!</b></div>");
			redirect('/administrative/');
		}
		// panggil fungsi ganti otorisasi
		$this->Administrative_Model->changeAuthorization($userid, $authorization);
		$this->session->set_flashdata('response',"<div class='alert alert-success'><b>You've Change User $userid Authorization</b></div>");
		redirect('/administrative/');
		// $this->load->view("signup");
		// $otorisasi = $this->User->setOtorisasi($username);
		/*$upt = "UPDATE user SET otorisasi = ?"*/
	}

	public function signupProc(){
		$session =$this->session->userdata('uName');
            if($session == false){
                redirect(base_url());
            }
        $this->load->model('User');
		$this->load->model('Administrative_Model');
		//check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
		$username = strip_tags($_POST['username']);
		$passwd = strip_tags($_POST['password']);
		//$fullname = strip_tags($_POST['fullname']);
		$salt = uniqid();
		$passwordFinal = md5($passwd.$salt);
		$fullname = $this->User->getPersData($username);
		
		if ($fullname['name'] == NULL) {
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>This Username Didn't Exist In Your SAP System!</b></div>");
			redirect('/administrative/');
		}

		$data = array(
			'username'=> $username,
			'fullname' => $fullname['name'],
			'otorisasi' => 'PEG',
			'status' => 'AKTIF',
			'password' => $passwordFinal,
			'salt' => $salt
			);
		$useravailability = $this->User->cekUser($username);
		if ($username != NULL && $passwd != NULL) {
			if ($username != NULL || $passwd != NULL){		
				if ($useravailability == $username ) {
				$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>User already exist!</b></div>");
				redirect('/administrative/');
				}
				$this->Administrative_Model->signup($data);
				$this->session->set_flashdata('response',"<div class='alert alert-success'><b>User $username Added!</b></div>");
				redirect('/administrative/');
			}else{$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Please input both forms!</b></div>");
				redirect('/administrative/');
			}
		}else{$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Please input both forms!</b></div>");
			redirect('/administrative/');
		}
		
		echo $rs;
	}

	public function signupMpyProc(){
		$session =$this->session->userdata('uName');
            if($session == false){
                redirect(base_url());
        }
        $this->load->model('User');
        //check status non aktif dan hapus session user!
		$status = $this->User->cekStatus($session);
		if($status == 'NON AKTIF'){
			$this->session->unset_userdata('uName');
			$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>Your Account is been frozed!</b></div>");
			redirect('gapura/');
		}
		//path of filesnya
		$config = array(
			'upload_path' => "./upload/excel",
			'allowed_types' => "xlsx",
			'overwrite' => TRUE,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)

			);
			$this->load->library('upload', $config);
			if($this->upload->do_upload())
			{
			$data = array('upload_data' => $this->upload->data());
			$file_name = $data['upload_data']['file_name']; //nama filenya
			}
			else
			{
				$this->session->set_flashdata('response',"<div class='alert alert-danger'><b>Please use excel files only!</b></div>");
				redirect('/administrative/');
				$error = array('error' => $this->upload->display_errors());
			} //returns array yg menyimpan seluruh data dari file
		

		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		//read only
		$objReader->setReadDataOnly(true);
		//load excel file
		$objPHPExcel = $objReader->load('upload/excel/'.$file_name);
		//jadi kalo gini bisa tapi cuma 1 data aja yang ke input ke dalem SQLnya
		$totalrows = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();//hitung banyak row
		$alertrows = $totalrows - 1;
		$objWorksheet = $objPHPExcel->setActiveSheetIndex(0);
		$this->load->model('User');
		$ctr=0;
		//loop data dalem excel
		for ($i=2; $i<=$totalrows; $i++) { 
			$username = strip_tags($objWorksheet->getCellByColumnAndRow(0,$i)->getValue());
			$password = strip_tags($objWorksheet->getCellByColumnAndRow(1,$i)->getValue());
			$otorisasi = strip_tags($objWorksheet->getCellByColumnAndRow(2,$i)->getValue());
			// Otorisasi yang akan dimasukkan ke database
			if(substr_count($otorisasi,"ADM")!=0){
				$strOtorisasi= $strOtorisasi."ADM";
			}
			if(substr_count($otorisasi,"HCM")!=0){
				$strOtorisasi= $strOtorisasi."-HCM";
			}
			else{
				$strOtorisasi= "PEG";
			}

			//fetch fullname dari SAP
			$fullname = $this->User->getPersData($username);
			$salt = uniqid();
			$passwordFinal = md5($password.$salt);


			if ($username == NULL || $password == NULL || $otorisasi == NULL) {
				$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>$ctr users added! But error occured in the following column. Please fill every column in your excel files correctly!</b></div>");
				redirect('/administrative/');
			}

			$useravailability = $this->User->cekUser($username);
			if ($useravailability == $username ) {
				$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>$ctr users added! Error occured at user $username because the username is already exist and We also stop the addition process from username $username! please update your excel files!</b></div>");
				redirect('/administrative/');
			}
			if ($fullname['name'] == NULL) {
				$this->session->set_flashdata('response',"<div class='alert alert-warning'><b>$ctr users added! Error occured at user $username because the username didn't exist in SAP System and We also stop the addition process from username $username! please update your excel files!</b></div>");
				redirect('/administrative/');
			}
			$data = array(
			'username'=> $username,
			'fullname' => $fullname['name'],
			'otorisasi' => $strOtorisasi,
			'status' => 'AKTIF',
			'password' => $passwordFinal,
			'salt' => $salt
			);
			$this->Administrative_Model->signupMpy($data);
			$ctr++; 
		}
		unlink('upload/excel/'.$file_name); //delete file setelah upload ke db
		$this->session->set_flashdata('response',"<div class='alert alert-success'><b>Succesfully Added $alertrows Users!</b></div>");
		redirect('/administrative/');
	}

	public function dashboard(){
		redirect('/dashboard/');
	}

	public function profile(){
		$this->load->view('biodata');
	}
}

?>