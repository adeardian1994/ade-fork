<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payslip extends CI_Model {	

	public function cekAktif($nopeg){
		$sap = new SAPConnection();
		$sap->Connect();
		$nomor = $nopeg;

		if($sap->getStatus() == SAPRFC_OK) $sap->open();
		if ($sap->GetStatus() != SAPRFC_OK ) {
		       #$sap->PrintStatus();
		       exit;
		    }
		// Cek apakah pegawai aktif
		$nowDate = (string)date("Ymd");

		$fce = $sap->NewFunction ("ZHR_GET_EMPLOYEE_DATA");
		if ($fce == false ) {
		       // $sap->PrintStatus();
		       exit;
		}

		$fce->PERSON_ID=$nomor;
		$fce->SELECTION_BEGIN=$nowDate;
		$fce->SELECTION_END=$nowDate;

		$fce->Call();
		if($fce->GetStatus() == SAPRFC_OK){
		  if($fce->PERSONAL_DATA["STATUS"]!="3"){
		    // echo "Anda bukan pegawai aktif";
		    // Echo $nowDate;
		    // echo $fce->PERSONAL_DATA["PERNR"];
		    $fce->Close();
		    return 0;
		    exit;
		  }
		  else{
		  	$fce->Close();
		  	return 1;
		  }
		}
		else{
		  $fce->PrintStatus();
		  $fce->Close();
		  echo "gajalan";
		}
	}

	public function ambilSeqNumber($nopeg){
		$sap = new SAPConnection();
		$sap->Connect();

		if($sap->getStatus() == SAPRFC_OK) $sap->open();
		if ($sap->GetStatus() != SAPRFC_OK ) {
		       #$sap->PrintStatus();
		       exit;
		    }
		// Untuk ambil nomor gaji ke-n
		$beginDate = "01.01.2015";
		$endDate = "31.12.9999";

		$seqNumber = array();
		$fce = $sap->NewFunction ("BAPI_GET_PAYROLL_RESULT_LIST");
	    if ($fce == false ) {
	       // $sap->PrintStatus();
	       exit;
	    }

		$fce->EMPLOYEENUMBER=$nopeg;
		$fce->FROMDATE=$beginDate;
		$fce->TODATE=$endDate;

		$fce->Call();
		if(isset($fce->RESULTS)){
		  $fce->RESULTS->Reset();
		   while ($fce->RESULTS->next()) {

		   	// Dibuat agar tanggal menjadi readable
		   	$tglraw =$fce->RESULTS->row["PAYDATE"];
		   	$sbsthn = substr($tglraw, 0,4);
		   	$sbsbln = substr($tglraw, 4,2);
		   	$bulan = $this->ubahBulan($sbsbln);

		   	// Rakit tulisan tanggalnya
		   	$stringTgl = $sbsthn." ".$bulan;

		   	$push = array($stringTgl,sprintf("%05d",$fce->RESULTS->row["SEQUENCENUMBER"]));
		    // echo $fce->RESULTS->row["SEQUENCENUMBER"]."<br>";
		    array_push($seqNumber, $push);
		    //array_push($seqNumber, $fce->RESULTS->row["PAYDATE"]);
		  }
		  return $seqNumber;
		}
		else{
		  $fce->PrintStatus();  
		}
	}

	public function loadPayslip($nopeg, $seqNumber){
		// konfigurasi koneksi SAP
		$config = array('ASHOST' => '192.168.5.119',
				'SYSNR' => '10',
				'CLIENT' => '010',
				'USER' => 'ASYSTDK',
				'PASSWD' => '87654321',);

		// Construct class, dan login
		$sap = new saprfc();
		$sap->setLoginData($config);
		$sap->login();

		// Panggil fungsi dan simpan ke variabel
		$hasil = $sap->callFunction("BAPI_GET_PAYSLIP", array(
				array("IMPORT","EMPLOYEENUMBER",$nopeg),
				array("IMPORT","SEQUENCENUMBER",$seqNumber),
				array("IMPORT","PAYSLIPVARIANT","GAPURA"),
				array("TABLE","PAYSLIP",array())
				)
				);
		$slip = $hasil['PAYSLIP'];
		return $hasil;
	}

	public function ubahBulan($stringBulan){
		if($stringBulan=="01"){
			return "Januari";
		}
		else if($stringBulan=="02"){
			return "Februari";
		}
		else if($stringBulan=="03"){
			return "Maret";
		}
		else if($stringBulan=="04"){
			return "April";
		}
		else if($stringBulan=="05"){
			return "Mei";
		}
		else if($stringBulan=="06"){
			return "Juni";
		}
		else if($stringBulan=="07"){
			return "Juli";
		}
		else if($stringBulan=="08"){
			return "Agustus";
		}
		else if($stringBulan=="09"){
			return "September";
		}
		else if($stringBulan=="10"){
			return "Oktober";
		}
		else if($stringBulan=="11"){
			return "November";
		}
		else if($stringBulan=="12"){
			return "Desember";
		}
	}
}
?>