<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CvModel extends CI_Model {	

	public function isiCV($nopeg){
		$sap = new SAPConnection();
		$sap->Connect();
		$hasil = array();

		if($sap->getStatus() == SAPRFC_OK) $sap->open();
		if ($sap->GetStatus() != SAPRFC_OK ) {
		       #$sap->PrintStatus();
		       exit;
		    }
		// Cek apakah pegawai aktif
		$nowDate = (string)date("Ymd");

		$fce = $sap->NewFunction ("ZHR_GET_BIODATA_GP");
		if ($fce == false ) {
		       // $sap->PrintStatus();
		       exit;
		}

		$fce->IM_PERNR = $nopeg;
		$fce->IM_BEGDA = $nowDate;
		$fce->IM_ENDDA = '99991231';

		$fce->Call();
		if($fce->GetStatus() == SAPRFC_OK){
			// Data pribadi
			$dataPribadi = $this->isiDataPribadi($fce);
			$hasil["dataPribadi"]=$dataPribadi;
			// array_push($hasil,"dataPribadi"=>$dataPribadi);
			// Riwayat edukasi
			$edukasi = $this->isiEdukasi($fce);
			$hasil["edukasi"] = $edukasi;
			//  Riwayat training
			$training = $this->isiTraining($fce);
			$hasil["training"] = $training;
			// Riwayat jabatan
			$jabatan = $this->isiRiwayatJabatan($fce);
			$hasil["jabatan"] = $jabatan;
			// Riwayat penugasan
			$penugasan = $this->isiRiwayatPenugasan($fce);
			$hasil["penugasan"] = $penugasan;
			// anggota keluarga
			$keluarga = $this->isiFamily($fce);
			$hasil["keluarga"] = $keluarga;
			// lisensi
			$lisensi = $this->isiLisensi($fce);
			$hasil["lisensi"] = $lisensi;
			// penghargaan
			$award = $this->isiAwards($fce);
			$hasil["award"] = $award;
			// punishment
			$punish = $this->isiPunishment($fce);
			$hasil["punishment"] = $punish;
			return $hasil;
		}
		else{
		  $fce->Close();
		  echo "gajalan";
		}
	}

	private function isiDataPribadi($fce){
		// $fce itu objek rfc SAP
		$temp = array(
				"nopeg"=>$fce->EX_DATA['PERNR'],
				"nama"=>$fce->EX_DATA['SNAME'],
				"ttl"=>$fce->EX_DATA['BIRTH'],
				"jabatan"=>$fce->EX_DATA['POSTXT'],
				"divisi"=>$fce->EX_DATA['ORGTXT'],
				"lokasi"=>$fce->EX_DATA['LOCATION'],
				"gender"=>$fce->EX_DATA['GENDER'],
				"agama"=>$fce->EX_DATA['RELIGION'],
				"hireDate"=>$fce->EX_DATA['HIREDATE'],
				"statusPeg"=>$fce->EX_DATA['EMP_STAT'],
				"statusNikah"=>$fce->EX_DATA['MAR_STAT'],
				"tglNikah"=>$fce->EX_DATA['MAR_DATE'],
				"edukasi"=>$fce->EX_DATA['EDUCATION'],
				"address1"=>$fce->EX_DATA['ADDRESS1'],
				"address2"=>$fce->EX_DATA['ADDRESS2'],
				"kota"=>$fce->EX_DATA['CITY'],
				"nomor"=>$fce->EX_DATA['PHONE']
			);
		return $temp;
	}

	private function isiEdukasi($fce){
		// $fce itu objek rfc SAP
		$fce->T_EDUCATION->Reset();
		$temp = array();
		while ($fce->T_EDUCATION->next()) {
			$row = array(
				"tingkat"=>$fce->T_EDUCATION->row['LEVEL'],
				"institusi"=>$fce->T_EDUCATION->row['INSTI'],
				"gelar"=>$fce->T_EDUCATION->row['BRANC'],
				"tahun"=>$fce->T_EDUCATION->row['YEAR']
			);
		array_push($temp, $row);	
		}
		return $temp;
	}

	private function isiTraining($fce){
		// $fce itu objek rfc SAP
		$fce->T_TRAINING->Reset();
		$temp = array();
		while ($fce->T_TRAINING->next()) {
			$row = array(
				"judul"=>$fce->T_TRAINING->row['STEXT'],
				"institusi"=>$fce->T_TRAINING->row['INSTI'],
				"mulai"=>$fce->T_TRAINING->row['BEGDA'],
				"selesai"=>$fce->T_TRAINING->row['ENDDA']
			);
		array_push($temp, $row);	
		}
		return $temp;
	}

	private function isiRiwayatJabatan($fce){
		// $fce itu objek rfc SAP
		$fce->T_EXPERIENCE->Reset();
		$temp = array();
		while ($fce->T_EXPERIENCE->next()) {
			$row = array(
				"judul"=>$fce->T_EXPERIENCE->row['STEXT'],
				"mulai"=>$fce->T_EXPERIENCE->row['BEGDA'],
				"selesai"=>$fce->T_EXPERIENCE->row['ENDDA']
			);
		array_push($temp, $row);	
		}
		return $temp;
	}

	private function isiRiwayatPenugasan($fce){
		// $fce itu objek rfc SAP
		$fce->T_ASSIGNMENT->Reset();
		$temp = array();
		while ($fce->T_ASSIGNMENT->next()) {
			$row = array(
				"judul"=>$fce->T_ASSIGNMENT->row['STEXT'],
				"mulai"=>$fce->T_ASSIGNMENT->row['BEGDA'],
				"selesai"=>$fce->T_ASSIGNMENT->row['ENDDA']
			);
		array_push($temp, $row);	
		}
		return $temp;
	}

	private function isiFamily($fce){
		// $fce itu objek rfc SAP
		$fce->T_FAMILY->Reset();
		$temp = array();
		while ($fce->T_FAMILY->next()) {
			$row = array(
				"hubungan"=>$fce->T_FAMILY->row['RELAT'],
				"nama"=>$fce->T_FAMILY->row['FCNAM'],
				"ttl"=>$fce->T_FAMILY->row['BIRTH']
			);
		array_push($temp, $row);	
		}
		return $temp;
	}

	private function isiLisensi($fce){
		// $fce itu objek rfc SAP
		$fce->T_LICENSE->Reset();
		$temp = array();
		while ($fce->T_LICENSE->next()) {
			$row = array(
				"judul"=>$fce->T_LICENSE->row['DESCRIPTION'],
				"tgl"=>$fce->T_LICENSE->row['DATE_ISSUE'],
				"nomorSeri"=>$fce->T_LICENSE->row['SK_NUMBER']
			);
		array_push($temp, $row);	
		}
		return $temp;
	}

	private function isiAwards($fce){
		// $fce itu objek rfc SAP
		$fce->T_AWARDS->Reset();
		$temp = array();
		while ($fce->T_AWARDS->next()) {
			$row = array(
				"tgl"=>$fce->T_AWARDS->row['DATUM'],
				"judul"=>$fce->T_AWARDS->row['AWDTX'],
				"deksripsi"=>$fce->T_AWARDS->row['ATEXT']
			);
		array_push($temp, $row);	
		}
		return $temp;
	}

	private function isiPunishment($fce){
		// $fce itu objek rfc SAP
		$fce->T_PUNISHMENT->Reset();
		$temp = array();
		while ($fce->T_PUNISHMENT->next()) {
			$row = array(
				"tgl"=>$fce->T_PUNISHMENT->row['PDATE'],
				"judul"=>$fce->T_PUNISHMENT->row['TPTXT'],
				"deksripsi"=>$fce->T_PUNISHMENT->row['JPTXT']
			);
		array_push($temp, $row);	
		}
		return $temp;
	}
}
?>